package qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActContracts {


    @JsonProperty("contractNumber")
    private String contractNumber;

    @JsonProperty("signDate")
    private Date signDate;

    @JsonProperty("actSignDate")
    private Date actSignDate;

    @JsonProperty("leasingGoodName")
    private String leasingGoodName;

    @JsonProperty("contractSum")
    private int contractSum;

    public String getContractNumber() { return contractNumber; }
    public Date getSignDate() { return signDate; }
    public Date getActSignDate() { return actSignDate; }
    public String getLeasingGoodName() { return leasingGoodName; }
    public int getContractSum() {return contractSum;}

    @Override
    public String toString() {
        return "ActContracts {" +
                "contractNumber = '" + contractNumber + '\'' +
                ",signDate = '" + signDate + '\'' +
                ",actSignDate = '" + actSignDate + '\'' +
                ",leasingGoodName = '" + leasingGoodName + '\'' +
                ",contractSum = '" + contractSum + '\'' +
                "}"
                ;
    }
}
