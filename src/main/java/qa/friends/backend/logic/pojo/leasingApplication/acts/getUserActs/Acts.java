package qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Acts {

    @JsonProperty("actId")
    private String actId;

    @JsonProperty("actNumber")
    private int actNumber;

    @JsonProperty("createDate")
    private Date createDate;

    @JsonProperty("totalAgentSum")
    private int totalAgentSum;

    @JsonProperty("totalContractsCount")
    private int totalContractsCount;

    @JsonProperty("periodFrom")
    private Date periodFrom;

    @JsonProperty("periodTo")
    private Date periodTo;

    @JsonProperty("actContracts")
    private List<ActContracts> actContracts;

    public String getActId() {return actId;}
    public int getActNumber() {return actNumber;}
    public Date getCreateDate() {return createDate;}
    public int getTotalAgentSum() {return totalAgentSum;}
    public int getTotalContractsCount() {return totalContractsCount;}
    public Date getPeriodFrom() {return getPeriodFrom();}
    public Date getPeriodTo() {return getPeriodTo();}
    public List<ActContracts> getActContracts() {return actContracts;}



    @Override
    public String toString() {
        return "Acts {" +
                "actId = '" + actId + '\'' +
                ",actNumber = '" + actNumber + '\'' +
                ",createDate = '" + createDate + '\'' +
                ",totalAgentSum = '" + totalAgentSum + '\'' +
                ",totalContractsCount = '" + totalContractsCount + '\'' +
                ",periodFrom = '" + periodFrom + '\'' +
                ",periodTo = '" + periodTo + '\'' +
                ",actContracts = '" + actContracts + '\'' +
                "}"
                ;

    }
}
