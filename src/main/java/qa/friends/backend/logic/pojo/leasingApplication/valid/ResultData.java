package qa.friends.backend.logic.pojo.leasingApplication.valid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultData {

    @JsonProperty("result")
    private String result;

    @JsonProperty("token")
    private String token;

    public String getResult() {
        return result;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "ResultData {" +
                "result = '" + result + '\'' +
                ",token = '" + token + '\'' +
                "}"
                ;
    }
}
