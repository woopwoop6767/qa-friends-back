package qa.friends.backend.logic.pojo.leasingApplication.valid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthTokenResponse {

    @JsonProperty("errorCode")
    private int errorCode;

    @JsonProperty("errorMessage")
    private Object errorMessage;

    @JsonProperty("resultData")
    private ResultData resultData;

    public int getErrorCode() {
        return errorCode;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public ResultData getResultData() {
        return resultData;
    }

    @Override
    public String toString() {
        return "AuthTokenResponse {" +
                "errorCode = '" + errorCode + '\'' +
                ",errorMessage = '" + errorMessage + '\'' +
                ",resultData = '" + resultData + '\'' +
                "}"
                ;
    }
}
