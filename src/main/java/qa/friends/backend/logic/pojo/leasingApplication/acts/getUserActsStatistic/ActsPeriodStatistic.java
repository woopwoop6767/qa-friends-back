package qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActsStatistic;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.crypto.Data;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActsPeriodStatistic {

    public ArrayList<Data> data;


     class Data {
        @JsonProperty("statType")
        private String statType;
        @JsonProperty("createdCnt")
        private int createdCnt;
        @JsonProperty("approvedCnt")
        private int approvedCnt;
        @JsonProperty("signedCnt")
        private int signedCnt;
        @JsonProperty("activeCnt")
        private int activeCnt;


        public String getStatType() { return statType; }
        public int getCreatedCnt() { return createdCnt; }
        public int getApprovedCnt() { return approvedCnt; }
        public int getSignedCnt() { return signedCnt; }
        public int getActiveCnt() { return activeCnt; }


        @Override
        public String toString() {
            return
                    "ActsPeriodStatistic {" +
                            "statType = '" + statType + '\'' +
                            ",createdCnt = '" + createdCnt + '\'' +
                            ",approvedCnt = '" + approvedCnt + '\'' +
                            ",signedCnt = '" + signedCnt + '\'' +
                            ",activeCnt = '" + activeCnt + '\'' +
                            "}";
        }


    }


//    @JsonProperty("statType")
//    private String statType;
//
//    @JsonProperty("createdCnt")
//    private int createdCnt;
//
//    @JsonProperty("approvedCnt")
//    private int approvedCnt;
//
//    @JsonProperty("signedCnt")
//    private int signedCnt;
//
//    @JsonProperty("activeCnt")
//    private int activeCnt;


//    public String getStatType() { return statType; }
//    public int getCreatedCnt() { return createdCnt; }
//    public int getApprovedCnt() { return approvedCnt; }
//    public int getSignedCnt() { return signedCnt; }
//    public int getActiveCnt() { return activeCnt; }
//
//
//    @Override
//    public String toString() {
//        return
//                "ActsPeriodStatistic {" +
//                        "statType = '" + statType + '\'' +
//                        ",createdCnt = '" + createdCnt + '\'' +
//                        ",approvedCnt = '" + approvedCnt + '\'' +
//                        ",signedCnt = '" + signedCnt + '\'' +
//                        ",activeCnt = '" + activeCnt + '\'' +
//                        "}";
//    }
}
