package qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActsStatistic;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonFormat(shape = JsonFormat.Shape.ARRAY)
public class UserActsStatistic {

    List<ActsPeriodStatistic> actsPeriodStatistics;


    public List<ActsPeriodStatistic> getActsPeriodStatistics() { return actsPeriodStatistics; }

    @Override
    public String toString() {
        return
                "ActsStatistic {" +
                        "ActsPeriodStatistic = '" + actsPeriodStatistics + '\'' +
                        "}";
    }
}
