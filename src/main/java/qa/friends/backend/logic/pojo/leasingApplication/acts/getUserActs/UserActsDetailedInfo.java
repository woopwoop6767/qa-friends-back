package qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserActsDetailedInfo {

    @JsonProperty("acts")
    private List<Acts> acts;

    public List<Acts> getActs() {
        return acts;
    }

    @Override
    public String toString() {
        return "UserActsDetailedInfo {" +
                "acts = '" + acts + '\'' +
                "}"
                ;
    }
}
