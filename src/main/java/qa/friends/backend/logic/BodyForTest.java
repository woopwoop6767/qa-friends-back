package qa.friends.backend.logic;

import java.util.HashMap;

public class BodyForTest {

    protected HashMap getBodyForAuthtorization(String mobilePhoneLogin, String smsCodePassword) {
        return new HashMap() {{
            put("mobilePhone", mobilePhoneLogin);
            put("pin", smsCodePassword);
        }};
    }
}
