package qa.friends.backend.logic.services;

import io.qameta.allure.Step;
import qa.friends.backend.logic.BodyForTest;
import qa.friends.backend.logic.api.Specification;
import qa.friends.backend.logic.pojo.leasingApplication.valid.AuthTokenResponse;
import qa.friends.backend.logic.pojo.leasingApplication.valid.ResultData;

import java.io.File;

import static io.restassured.RestAssured.filters;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class LeasingApplicationService extends BodyForTest implements Specification {

    private AuthTokenResponse authTokenResponse;

    private AuthTokenResponse tokenResponse(String mobilePhoneLogin, String smsCodePassword) {
        if (authTokenResponse == null) {

            authTokenResponse = given()
                    .spec(getRequestSpecification("/agent-bouncer/v1/token"))
                    .body(getBodyForAuthtorization(mobilePhoneLogin, smsCodePassword))
                    .post()
                    .then()
                    .log().all()
                    .spec(getResponseSpecification(false))
                    .body(matchesJsonSchema(new File("src/main/resources/jsonSchema/valid/post_authtorization.json")))
                    .extract().body().as(AuthTokenResponse.class)
                    ;
        }
        return authTokenResponse;
    }

    @Step("I get authorization token")
    public String getAuthToken(String mobilePhoneLogin, String smsCodePassword) {
        return tokenResponse(mobilePhoneLogin, smsCodePassword)
                .getResultData()
                .getToken()
                ;
    }
}
