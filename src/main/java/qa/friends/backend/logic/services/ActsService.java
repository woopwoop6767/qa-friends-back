package qa.friends.backend.logic.services;


import qa.friends.backend.logic.api.Specification;
import qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActs.UserActsDetailedInfo;
import qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActsStatistic.ActsPeriodStatistic;
import qa.friends.backend.logic.pojo.leasingApplication.acts.getUserActsStatistic.UserActsStatistic;

import java.io.File;
import java.util.List;
import java.util.NoSuchElementException;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class ActsService implements Specification {

    private UserActsDetailedInfo userActsDetailedInfo;
    private ActsPeriodStatistic actsPeriodStatistics;

    public UserActsDetailedInfo getUserActs(String authToken) {
        if (userActsDetailedInfo == null) {
            userActsDetailedInfo = given()
                    .spec(getRequestSpecification("act/v1/act"))
                    .header("authorization", "Bearer " + authToken)
                    .get()
                    .then()
                    .log().all()
                    .spec(getResponseSpecification(false))
                    .body(matchesJsonSchema(new File("src/main/resources/jsonSchema/valid/get_act_v1_act.json")))
                    .extract().body().as(UserActsDetailedInfo.class)
                    ;
        }
        return userActsDetailedInfo;
    }

    public ActsPeriodStatistic getUserUserActsStatistic(String authToken) {
        if (actsPeriodStatistics == null) {
              actsPeriodStatistics = given()
                    .spec(getRequestSpecification("act/v1/stat"))
                    .header("authorization", "Bearer " + authToken)
                    .get()
                    .then()
                    .log().all()
                    .spec(getResponseSpecification(false))
                    .extract().body().as(ActsPeriodStatistic.class)
                     ;
        }
        return actsPeriodStatistics;
    }


    public String getStartType(String authToken) {
        return getUserUserActsStatistic(authToken).data.;
    }

    public String getContractNumber(String authToken) {
        return getUserActs(authToken)
                .getActs()
                .stream().filter(val -> val.getActId().toLowerCase().contains("bdd5ff2c-2741-45e6-b7d1-dfb1a93c2b1a"))
                .findAny().orElseThrow(NoSuchElementException::new)
                .getActContracts()
                .stream().findAny().orElseThrow(NoSuchElementException::new)
                .getContractNumber()
                ;
    }


}
