package qa.friends.backend.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import qa.friends.backend.logic.services.ActsService;
import qa.friends.backend.logic.services.LeasingApplicationService;

public class test002UserUserActsStatistic {


    private String mobilePhoneLogin = "9683333423";
    private String smsCodePassword = "111111";
    private String authToken;

    private LeasingApplicationService leasingApplicationService;
    private ActsService actsService;

    @BeforeMethod
    void setUp() {

        actsService = new ActsService();
        leasingApplicationService = new LeasingApplicationService();
    }

    @Test(description = "Check valid acts data response (act/v1/act)")
    void test() {

        authToken = leasingApplicationService
                .getAuthToken(mobilePhoneLogin, smsCodePassword);

        System.out.println(actsService
                .getUserUserActsStatistic(authToken));




    }
}
