package qa.friends.backend.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import qa.friends.backend.logic.api.Specification;
import qa.friends.backend.logic.pojo.leasingApplication.valid.AuthTokenResponse;
import qa.friends.backend.logic.pojo.leasingApplication.valid.ResultData;
import qa.friends.backend.logic.services.ActsService;
import qa.friends.backend.logic.services.LeasingApplicationService;

public class test001UserActs implements Specification {

    private String mobilePhoneLogin = "9683333423";
    private String smsCodePassword = "111111";
    private String authToken;

    private LeasingApplicationService leasingApplicationService;
    private ActsService actsService;
    private AuthTokenResponse authTokenResponse;
    private ResultData resultData;

    @BeforeMethod
    void setUp() {

        actsService = new ActsService();
        leasingApplicationService = new LeasingApplicationService();
        authTokenResponse = new AuthTokenResponse();
        resultData = new ResultData();
    }

    @Test(description = "Check valid acts data response (act/v1/act)")
    void test() {

        authToken = leasingApplicationService
                .getAuthToken(mobilePhoneLogin, smsCodePassword);

        actsService.getUserActs(authToken);


    }

}
